#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
POOL = eu1.ethermine.org:4444
WALLET = 0x0fa94ee9a37702c902b1200084c643e047b11ce1
WORKER = cov19
EPSW = x
COIN = eth
#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./PhoenixMiner && ./PhoenixMiner -pool $POOL -wal $WALLET -worker $WORKER -epws $EPSW -mode 1 -log 0 -mport 0 -etha 0 -ftime 55 -retrydelay 1 -tt 79 -tstop 89  -coin $COIN $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./PhoenixMiner -pool $POOL -wal $WALLET -worker $WORKER -epws $EPSW -mode 1 -log 0 -mport 0 -etha 0 -ftime 55 -retrydelay 1 -tt 79 -tstop 89  -coin $COIN $@
done
